import {Item, IItemProperties} from "../../models";
import {ItemCreationError} from "../../exceptions/itemCreationError";

let itemProp: IItemProperties;
let item: Item;

beforeEach(() => {
    let name: string = "pc";
    let content: string = "acheter un pc";
    itemProp = {
        name,
        content,
    }
    item = Item.create(itemProp);
});

describe("Testing _item.old.model.ts => isValidInsert()", () => {

    it("should return true since all fields respects validation rules", () => {
        expect(Item.isValidInsert(item)).toBe(true);
    });

    it("should return false since name is empty", () => {
        item.name = "";
        expect(Item.isValidInsert(item)).toBe(false);
    });

    it("should return flase if content > 1000 caracters", () => {
        for (let i = 0; i < 1003; i++) {
            item.content = item.content + i.toString();
        }
        expect(Item.isValidInsert(item)).toBe(false);
    });
});


describe("Testing _item.old.model.ts => create()", () => {

    it("should return new user since all fields respects validation rules", () => {
        const itemToBeCreated: Item = Item.create(itemProp);
        expect(Item.create(item)).toStrictEqual(itemToBeCreated);
    });

    it("thrown error should contain ValidationError since field is invalid", () => {
        expect(() => {
            itemProp.name = "";
            Item.create(itemProp)
        }).toThrow(ItemCreationError);
    });

});
