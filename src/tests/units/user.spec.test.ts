import {ITodoListProperties, IUserProperties, TodoList, User} from "../../models";
import {ValidationError} from "../../exceptions";
import {ToDoListCreationError} from "../../exceptions/toDoListCreationError";

let user: User;
let userProps: IUserProperties;

beforeEach(() => {
    let firstName: string = "Jean";
    let lastName: string = "Dupont";
    let email: string = "jdupont@myges.fr";
    let password: string = "test1234";
    let birthDate: Date = new Date("2000-01-01");
    userProps = {
        firstName,
        lastName,
        email,
        password,
        birthDate
    };
    user = User.create(userProps);
});

describe("Testing _user.old.model.ts => isValid()", () => {

    it("should return true since all fields respects validation rules", () => {
        expect(User.isValid(user)).toBe(true);
    });

    it("should return false since firstName is empty", () => {
        user.firstName = ""
        expect(User.isValid(user)).toBe(false);
    });

    it("should return false since lastName is empty", () => {
        user.lastName = ""
        expect(User.isValid(user)).toBe(false);
    });

    it("should return false since email is empty", () => {
        user.email = ""
        expect(User.isValid(user)).toBe(false);
    });

    it("should return false since email is not well formatted", () => {
        user.email = "wquach myges.fr"
        expect(User.isValid(user)).toBe(false);
    });

    it("should return false since password is empty", () => {
        user.password = ""
        expect(User.isValid(user)).toBe(false);
    });

    it("should return false since password is less than 8 characters", () => {
        user.password = "1234567"
        expect(User.isValid(user)).toBe(false);
    });

    it("should return false since password is more than 40 characters", () => {
        user.password = "12345678901234567890123456789012345678901"
        expect(User.isValid(user)).toBe(false);
    });
});


describe("Testing _user.old.model.ts => create()", () => {

    it("should return new user since all fields respects validation rules", () => {
        const correctUser: User = User.create(userProps);
        expect(User.create(userProps)).toStrictEqual(correctUser);
    });

    it("should throw UserCreationError since field is invalid", () => {
        user.email = "";
        expect(() => {
            User.create(user);
        }).toThrow(ValidationError);
    });

});

describe("Testing _user.old.model.ts => createToDoList()", () => {
    const todoListProps: ITodoListProperties = {
        label: "My ToDoList",
        user: user
    }

    it("should return new todolist since user didn't have one", () => {
        const todoListToBeCreated: TodoList = TodoList.create(todoListProps);
        expect(user.createToDoList(todoListProps)).toStrictEqual(todoListToBeCreated);
    });

    it("should throw ToDoCreationError since user already has a todolist", () => {
        user.toDoList = TodoList.create(todoListProps);
        expect(() => {
            user.createToDoList(todoListProps);
        }).toThrow(ToDoListCreationError);
    });

    it("should throw ToDoCreationError since user is invalid", () => {
        user.email = "";
        expect(() => {
            user.createToDoList(todoListProps);
        }).toThrow(ToDoListCreationError);
    });

});
