import {IItemProperties, Item, ITodoListProperties, TodoList, User} from "../../models";
import {ToDoListCreationError} from "../../exceptions/toDoListCreationError";
import {ToDoListAddItemError} from "../../exceptions/todoListAddItemError";
import {EmailSenderService} from "../../services/EmailSenderService";

let user: User;
let todoListProps: ITodoListProperties;
let todoList: TodoList;
let item: Item;

beforeEach(() => {
    user = User.create({
        email: "jdupont@myges.fr",
        firstName: "Jean",
        lastName: "Dupont",
        password: "test1234",
        birthDate: new Date("2000-01-01")
    });
    todoListProps = {
        label: "fourniture pc",
        user: user
    }
    todoList = TodoList.create(todoListProps);

    let itemProps: IItemProperties = {
        name: "pc",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt facilisis urna, vitae gravida nulla posuere eu. Sed vitae orci non metus malesuada semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos."
    }
    item = Item.create(itemProps);
    todoList.addItem(item);
    user.toDoList = todoList;
});

describe("Testing _todoList.old.model.ts => isValidInsert()", () => {

    it("should return true since name is not empty and item list is not full", () => {
        expect(TodoList.isValidInsert(todoList)).toBe(true);
    });

    it("should return false since label is empty", () => {
        todoList.label = "";
        expect(TodoList.isValidInsert(todoList)).toBe(false);
    });

    it("should return false since list is full", () => {
        todoList.items = [...[...Array(15)]].map((value, index) => {
            // Creates 15 items
            return new Item({
                name: `Name ${index}`,
                content: `Content ${index}`
            })
        });
        expect(TodoList.isValidInsert(todoList)).toBe(false);
    });
});

describe("Testing _todoList.old.model.ts => create()", () => {

    it("should return new todolist since properties respect validation rules", () => {
        const todoListToBeCreated = new TodoList(todoListProps);
        expect(TodoList.create(todoListProps)).toStrictEqual(todoListToBeCreated);
    });

    it("should throw ToDoCreationError since todo list props are invalid (isValidInsert(props))", () => {
        todoList.items = [...Array(15)].map((value, index) => {
            // Creates 15 items
            return new Item({
                name: `Name ${index}`,
                content: `Content ${index}`
            })
        });
        expect(() => {
            TodoList.create(todoList);
        }).toThrow(ToDoListCreationError);
    });
});

describe("Testing _todoList.old.model.ts => addItem()", () => {

    beforeEach(() => {
        user = User.create({
            email: "blabla@gmail.com",
            firstName: "bla",
            lastName: "blablou",
            password: "pwd1234567",
            birthDate: new Date("2000-01-01")
        });
        todoList = TodoList.create({
            label: "fourniture pc",
            user: user
        });

        let name: string = "pc";
        let content: string = "acheter un pc";
        let createdAt: Date = new Date(2020, 5, 4, 19, 10, 0);
        const itemProp = {
            name,
            content,
            createdAt
        }
        item = new Item(itemProp);
    });

    it("should insert since all fields respects validation rules", () => {
        todoList.addItem(item);
        expect(todoList.items!.length).toStrictEqual(1);
        expect(todoList.items![0]).toStrictEqual(item);
    });

    it("should not insert and throw ToDoListAddItemError since we insert 2 times the same item", () => {
        expect(() => {
            todoList.addItem(item);
            todoList.addItem(item);
        }).toThrow(ToDoListAddItemError);
    });

    it("should not insert and throw ToDoListAddItemError since we insert 2 items with the same name", () => {
        expect(() => {
            todoList.addItem(item);
            todoList.addItem(new Item({
                name: "pc",
                content:"un nouveau pc pour moi"
            }));
        }).toThrow(ToDoListAddItemError);
    });

    it("should not insert and throw ToDoListAddItemError since item has an empty name (Item.isValidInsert())", () => {
        expect(() => {
            item.name = "";
            todoList.addItem(item);
        }).toThrow(ToDoListAddItemError);
    });

    it("should not insert and throw ToDoListAddItemError since time between add < 30min", () => {
        item.createdAt = new Date();
        todoList.items.push(item);
        expect(() => {
            todoList.addItem(new Item({
                name: "new item",
                content: "created after first item but before the 30 min interval"
            }));
        }).toThrow(ToDoListAddItemError);
    });

    it("should not insert and throw ToDoListAddItemError since last add was less than 30 min ago", () => {
        let itemToAdd = new Item({
            name: "new item",
            content: "created after first item but before the 30 min interval"
        });

        item.createdAt = new Date();
        item.createdAt.setMinutes(item.createdAt.getMinutes() -10);
        todoList.items.push(item);
        expect(() => {
            todoList.addItem(itemToAdd);
        }).toThrow(ToDoListAddItemError);
    });

    it("should throw  false since list is full", () => {
        todoList.items = [...Array(15)].map((value, index) => {
            // Creates 15 items
            return new Item({
                name: `Name ${index}`,
                content: `Content ${index}`
            })
        });
        let itemToAdd = new Item({
            name: "new item",
            content: "new item content"
        });
        itemToAdd.createdAt.setMinutes(itemToAdd.createdAt.getMinutes() + 40)
        expect(() => {
            todoList.addItem(itemToAdd);
        }).toThrow(ToDoListAddItemError);
    });


    it("should send email since added item is the 8th of the todolist", () => {
        const spyOnSendEmail = jest.spyOn(EmailSenderService, 'sendEmail').mockImplementation();
        todoList.items = [];
        todoList.items = [...Array(7)].map((value, index) => {
            // Creates 7 items
            let item: Item = Item.create({
                name: `Name ${index}`,
                content: `Content ${index}`
            });
            item.createdAt = new Date("2020-01-01");
            return item;
        });
        let itemToAdd = new Item({
            name: "8th item",
            content: "8th item content"
        });
        todoList.addItem(itemToAdd);

        expect(spyOnSendEmail).toHaveBeenCalledTimes(1);
        expect(spyOnSendEmail).toHaveBeenCalledWith(user);

    });
});
