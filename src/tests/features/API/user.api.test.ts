import {UserController} from "../../../controllers/User.controller";
import Express from "express";
import supertest from "supertest";
import sqlite3 from "sqlite3";
import {buildRoutes} from "../../../routes";
import {ToDoListController} from "../../../controllers/ToDoList.controller";
import { ToDoListInstance } from "../../../models/toDoList.model";
import {UserInstance} from "../../../models/user.model";

const app = Express();
app.use(Express.json());
buildRoutes(app);

let database: sqlite3.Database = new sqlite3.Database(":memory:", () => console.log(`DB Connection opened.`));

let userController: UserController;
let toDoListController: ToDoListController;

let user: [UserInstance, boolean];
const userId = 1;

let toDoList: [ToDoListInstance, boolean];
const toDoListId = 1;
const todoListLabel = "Testing todolist";

beforeAll(async (done) => {
    userController = await UserController.getInstance();
    toDoListController = await ToDoListController.getInstance();
    user = await userController.create({
        id: userId,
        email: "testing@gmail.com",
        lastName: "testing",
        firstName: "dupont",
        birthDate: new Date(),
        password: "testing123"
    });

    toDoList = await toDoListController.create({
        id: toDoListId,
        label: todoListLabel
    });
    done();
})

describe("Testing API - Delete item of user's ToDoList => DELETE /user/:userId/:itemId/itemToDoList", () => {

    beforeEach(async (done) => {
        await user[0].setToDoList(toDoList[0] ? toDoList[0] : undefined);
        await toDoList[0].setUser(user[0] ? user[0] : undefined);
        done();
    })

    test("should return 200 & success message when deleting user's todolist item", async () => {
        await userController.addItemToDoList("1", {
            name: "Item",
            content: "Item content"
        });
        await supertest(app).delete(`/user/1/1/itemToDoList`)
            .then((response) => {
                const responseExpected = {
                    message: 'updated',
                    data: {
                        id: 1,
                        label: 'Testing todolist',
                        deletedAt: null,
                        UserId: 1,
                        Items: []
                    }
                }
                // => Check status code & headers & text
                expectStatusCodeAndContentType(response, 200, 'application/json; charset=utf-8');
                delete response.body.data["createdAt"];
                delete response.body.data["updatedAt"];
                expect(response.body).toEqual(responseExpected);
            });
    });

    test("should return 403 & message saying that user doesn't have asked item in his todolist", async () => {
        await user[0].setToDoList(toDoList[0] ? toDoList[0] : undefined);
        await toDoList[0].setUser(user[0] ? user[0] : undefined);
        await supertest(app).delete(`/user/1/1/itemToDoList`)
            .then((response) => {
                // => Check status code & headers & text
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
                expect(response.body).toEqual("ValidationError: Item doesn't exist in user's todolist");
            });
    });

    test("should return 500 & message saying that user didn't have a todolist", async () => {
        await user[0].setToDoList(undefined);
        await toDoList[0].setUser(undefined);
        await supertest(app).delete(`/user/1/1/itemToDoList`)
            .then((response) => {
                // => Check status code & headers & text
                expectStatusCodeAndContentType(response, 500, 'text/html; charset=utf-8');
                expect(response.text).toEqual("L'utilisateur n'a pas de ToDoList.");
            });
    });

    test("should return 403 & error message because user doesnt exist", async () => {
        await supertest(app).delete(`/user/2/1/itemToDoList`)
            .then((response) => {
                // Check data
                expect(response.text).toBe(`"Error: Couldn't find user 2 => sequelize error"`);
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });
});

describe("Testing API - Adding item to ToDoList => POST /user/:userId/itemToDoList", () => {
    beforeEach(async () => {
        try {
            await user[0].setToDoList(toDoList[0] ? toDoList[0] : undefined);
            await toDoList[0].setUser(user[0] ? user[0] : undefined);
        } catch(e) {
            console.log(e.message);
            console.log(e.stackTrace);
        }
    });

    test("should return 200 & user's updated toDoList with new item", async () => {
        await supertest(app).post(`/user/1/itemToDoList`)
            .send({
                name: "Nouvel item",
                content: "Contenu du nouvel item"
            })
            .then((response) => {
                response = expectParanoidProperties(response);

                const responseExpected = {
                    UserId: userId,
                    id: toDoListId,
                    label: todoListLabel,
                    Items: [
                        {
                            content: "Contenu du nouvel item",
                            name: "Nouvel item"
                        }
                    ],
                }

                // Check data
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 201, 'application/json; charset=utf-8');


                // => Check body
                expect(response.body).toEqual(responseExpected);
                expect(response.body.id).toBe(toDoListId);
                expect(response.body.label).toBe(todoListLabel);
            });
    });

    test("should return 403 because of waiting 30min more", async () => {
        await supertest(app).post(`/user/1/itemToDoList`)
            .send({
                name: "Nouvel item",
                content: "Contenu du nouvel item"
            })
        await supertest(app).post(`/user/1/itemToDoList`)
            .send({
                name: "Nouvel item",
                content: "Contenu du nouvel item"
            })
            .then((response) => {
                console.log(response.body);

                // Check data
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });

    test("should return 400 & error message with userid not int", async () => {
        await supertest(app).post(`/user/test/itemToDoList`)
            .send({
                name: todoListLabel
            })
            .then((response) => {
                // Check data
                expect(response.text).toBe("Un paramètre est manquant ! (userId ou toDoListLabel)");
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 400, 'text/html; charset=utf-8');
            });
    });

    test("should return 400 & error message with invalid body parameter : name and description", async () => {
        await supertest(app).post(`/user/1/itemToDoList`)
            .then((response) => {
                // Check data
                expect(response.text).toBe("Un paramètre est manquant ! (userId ou toDoListLabel)");
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 400, 'text/html; charset=utf-8');
            });
    });

    test("should return 400 & error message with invalid body parameter - desciption > 1000 caracters", async () => {
        await supertest(app).post(`/user/1/itemToDoList`)
            .send({
                name: todoListLabel,
                content: "should return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caractersshould return 400 & error message with invalid body parameter - desciption > 1000 caracters"
            })
            .then((response) => {
                // Check data
                expect(response.text).toBe("\"ValidationError: limit of 1000 caracters in description\"");
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });

    test("should return 400 & error message because user doesnt exist", async () => {
        await supertest(app).post(`/user/2/itemToDoList`)
            .send({
                toDoListLabel: todoListLabel
            })
            .then((response) => {
                // Check data
                expect(response.text).toBe(`Un paramètre est manquant ! (userId ou toDoListLabel)`);
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 400, 'text/html; charset=utf-8');
            });
    });

    test("should return 403 & error message because user doesn't have todolist", async () => {
        await supertest(app).post(`/user/2/todolist`)
            .send({
                toDoListLabel: todoListLabel
            })
            .then((response) => {
                // Check data
                expect(response.text).toBe(`"Error: Couldn't find user 2 => sequelize error"`);
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });
});

describe("Testing API - Update item to ToDoList => PUT /user/:userId/itemToDolist", () => {
    beforeEach(async () => {
        try {
            await user[0].setToDoList(toDoList[0] ? toDoList[0] : undefined);
            await toDoList[0].setUser(user[0] ? user[0] : undefined);
            await supertest(app).post(`/user/1/itemToDoList`)
                .send({
                    name: "Nouvel item",
                    content: "Contenu du nouvel item"
                })
        } catch(e) {
            console.log(e.message);
            console.log(e.stackTrace);
        }
    });

    test("should return 200 & user's updated toDoList with new item", async () => {
        await supertest(app).put(`/user/1/itemToDoList`)
            .send({
                id: 1,
                name: "Nouvel itzesqem",
                content: "Contenu du nouvel item"
            })
            .then((response) => {
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 200, 'application/json; charset=utf-8');
            });
    });

    test("should return 403 & error message with invalid body parameter", async () => {
        await supertest(app).put(`/user/test/itemToDoList`)
            .send({
                name: todoListLabel
            })
            .then((response) => {
                // Check data
                expect(response.text).toBe("\"SequelizeDatabaseError: SQLITE_ERROR: no such column: NaN\"");
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });

    test("should return 400 & error message because user doesnt exist", async () => {
        await supertest(app).put(`/user/2/itemToDoList`)
            .send({
                toDoListLabel: todoListLabel
            })
            .then((response) => {
                // Check data
                expect(response.text).toBe(`\"Error: Couldn't find user 2 => sequelize error\"`);
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });

});

describe("Testing API - Getting user's ToDoList =>  GET /user/:userId/todolist", () => {

    beforeEach(async (done) => {
        await user[0].setToDoList(toDoList[0] ? toDoList[0] : undefined);
        await toDoList[0].setUser(user[0] ? user[0] : undefined);
        done();
    });

    test("should return 200 & user's toDoList", async () => {
        await supertest(app).get(`/user/1/todolist`)
            .then((response) => {
                const responseExpected = {
                    UserId: 1,
                    id: toDoListId,
                    label: todoListLabel
                }

                // Check data
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 200, 'application/json; charset=utf-8');

                // => Check paranoid props
                response = expectParanoidProperties(response);

                // => Check body
                expect(response.body).toEqual(responseExpected);
                expect(response.body.id).toBe(toDoListId);
                expect(response.body.label).toBe(todoListLabel);

            });
    });

    test("should return 400 & error message because userid not int", async () => {
        await supertest(app).get(`/user/test/todolist`)
            .then((response) => {
                // Check data
                expect(response.text).toBe("Le paramètre userId dans le corps de la requête n'est pas de type entier !");
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 400, 'text/html; charset=utf-8');
            });
    });

    test("should return 403 & error message because user doesnt exist", async () => {
        await supertest(app).get(`/user/2/todolist`)
            .then((response) => {
                // Check data
                expect(response.text).toBe(`"Error: Couldn't find user 2 => sequelize error"`);
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });

});

describe("Testing API - Creating new ToDoList for user => POST /user/:userId/todolist", () => {

    test("should return 201 & user's new toDoList", async () => {
        await supertest(app).post(`/user/1/todolist`)
            .send({
                toDoListLabel: todoListLabel
            })
            .then((response) => {
                const responseExpected = {
                    UserId: userId,
                    id: toDoListId,
                    label: todoListLabel
                }

                // Check data
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 201, 'application/json; charset=utf-8');

                // => Check paranoid props
                response = expectParanoidProperties(response);

                // => Check body
                expect(response.body).toEqual(responseExpected);
                expect(response.body.id).toBe(toDoListId);
                expect(response.body.label).toBe(todoListLabel);
            });
    });

    test("should return 400 & error message because userid not int", async () => {
        await supertest(app).post(`/user/test/todolist`)
            .send({
                toDoListLabel: todoListLabel
            })
            .then((response) => {
                // Check data
                expect(response.text).toBe("Le paramètre userId dans le corps de la requête n'est pas de type entier !");
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 400, 'text/html; charset=utf-8');
            });
    });

    test("should return 400 & error message because of invalid body parameter", async () => {
        await supertest(app).post(`/user/1/todolist`)
            .then((response) => {
                // Check data
                expect(response.text).toBe("Un paramètre est manquant ! (userId ou toDoListLabel)");
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 400, 'text/html; charset=utf-8');
            });
    });

    test("should return 403 & error message because user doesnt exist", async () => {
        await supertest(app).post(`/user/2/todolist`)
            .send({
                toDoListLabel: todoListLabel
            })
            .then((response) => {
                // Check data
                expect(response.text).toBe(`"Error: Couldn't find user 2 => sequelize error"`);
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });
});

describe("Testing API - Delete user's ToDoList => DELETE /user/:userId/todolist", () => {

    test("should return 200 & success message when deleting user's todolist", async () => {
        await user[0].setToDoList(toDoList[0] ? toDoList[0] : undefined);
        await toDoList[0].setUser(user[0] ? user[0] : undefined);
        await supertest(app).delete(`/user/1/todolist`)
            .then((response) => {
                // => Check status code & headers & text
                expectStatusCodeAndContentType(response, 200, 'text/html; charset=utf-8');
                expect(response.text).toEqual(`Deleted user's toDoList`);
            });
    });

    test("should return 200 & message saying that user didn't have a todolist", async () => {
        await supertest(app).delete(`/user/1/todolist`)
            .then((response) => {
                // => Check status code & headers & text
                expectStatusCodeAndContentType(response, 200, 'text/html; charset=utf-8');
                expect(response.text).toEqual(`User didn't have a toDoList`);
            });
    });

    test("should return 400 & error message because userid not int", async () => {
        await supertest(app).delete(`/user/test/todolist`)
            .then((response) => {
                // Check data
                expect(response.text).toBe("Le paramètre userId dans le corps de la requête n'est pas de type entier !");
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 400, 'text/html; charset=utf-8');
            });
    });


    test("should return 403 & error message because user doesnt exist", async () => {
        await supertest(app).delete(`/user/2/todolist`)
            .then((response) => {
                // Check data
                expect(response.text).toBe(`"Error: Couldn't find user 2 => sequelize error"`);
                // => Check status code & headers
                expectStatusCodeAndContentType(response, 403, 'application/json; charset=utf-8');
            });
    });
});



function expectStatusCodeAndContentType(response: supertest.Response, expectedStatusCode: number, expectedContentType: string) {
    expect(response.statusCode).toBe(expectedStatusCode);
    expect(response.headers["content-type"]).toBe(expectedContentType);
}

function expectParanoidProperties(response: supertest.Response): supertest.Response {
    expect(response.body).toHaveProperty("createdAt");
    expect(response.body).toHaveProperty( "deletedAt");
    expect(response.body).toHaveProperty("updatedAt");
    delete response.body["createdAt"];
    delete response.body["updatedAt"];
    delete response.body["deletedAt"];
    return response;
}
