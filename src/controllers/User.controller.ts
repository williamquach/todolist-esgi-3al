import {ModelCtor} from "sequelize";
import {SequelizeManager} from "../models";
import {IUserProperties, UserCreationProps, UserInstance} from "../models/user.model";
import {ToDoListInstance} from "../models/toDoList.model";
import {ToDoListController} from "./ToDoList.controller";
import {ToDoListCreationError} from "../exceptions/toDoListCreationError";
import {UserRetrivingException} from "../exceptions/userRetrievingException";
import {ItemCreationProps, ItemInstance, IItemProperties} from "../models/item.model";
import {ToDoListAddItemError} from "../exceptions/todoListAddItemError";
import {ItemDeleteError} from "../exceptions/itemDeleteError";

export class UserController {
    User: ModelCtor<UserInstance>;
    Items: ModelCtor<ItemInstance>;
    ToDo: ModelCtor<ToDoListInstance>;

    private static instance: UserController;

    public static async getInstance(): Promise<UserController> {
        if(this.instance === undefined) {
            const manager = await SequelizeManager.getInstance();
            UserController.instance = new UserController(manager.User, manager.Item, manager.ToDoList);
        }
        return UserController.instance;

    }

    private constructor(User: ModelCtor<UserInstance>, Item: ModelCtor<ItemInstance>, ToDo: ModelCtor<ToDoListInstance>) {
        this.User = User;
        this.Items = Item;
        this.ToDo = ToDo;
    }

    public async create(props: UserCreationProps): Promise<[UserInstance, boolean]> {
        return await this.User.findOrCreate({
            where: {
                ...props
            },
        });
    }

    public async deleteById(id: string) {
        await this.User.destroy({
            where: {
                id
            }
        });
    }

    public async update(props: IUserProperties) {
        try {
            await this.User.update({
                ...props
            }, {
                where: {
                    firstName: props.firstName,
                }
            });
            // return user;
        } catch (e) {
            return;
        }
    }

    public async findAll(start: number, end: number): Promise<UserInstance[] | null> {
        return await this.User.findAll({
            offset: start,
            limit: end,
        });
    }

    public async findById(id: number): Promise<UserInstance | null> {
        return await this.User.findOne({
            where: {
                id
            },
        });
    }

    public async getToDoList(userId: string): Promise<ToDoListInstance | null> {
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }

        return await user.getToDoList();
    }

    public async addToDoList(userId: string, label: string): Promise<ToDoListInstance | null> {
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }

        const userToDoList: ToDoListInstance | null = await user.getToDoList();
        if(userToDoList != null) {
            // Already has a ToDoList
            return userToDoList;
        }
        else {
            // Doesn't have a ToDoList yet
            const toDoListController: ToDoListController = await ToDoListController.getInstance();
            const newToDoList: [ToDoListInstance, boolean] = await toDoListController.create({
                label
            });
            if(newToDoList[0] == null) {
                throw new ToDoListCreationError("Couldn't create a new ToDoList => sequelize error");
            }
            else {
                await user.setToDoList(newToDoList[0]);
                await newToDoList[0].setUser(user);
                return newToDoList[0];
            }
        }
    }

    public async removeToDoList(userId: string): Promise<boolean> {
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }

        const userToDoList: ToDoListInstance | null = await user.getToDoList();
        const toDoListController: ToDoListController = await ToDoListController.getInstance();
        if(userToDoList != null) {
            // Already has a ToDoList
            await user.setToDoList(undefined);
            return await toDoListController.delete(userToDoList.id) != null;
        }
        else {
            // Didn't have a ToDoList
            return false;
        }
    }

    public async addItemToDoList(userId: string, newItem: IItemProperties): Promise<ToDoListInstance | null> {
        if (!await this.checkAddItem(newItem, userId)) {
            return null;
        }
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }

        let userToDoList: ToDoListInstance | null = await user.getToDoList();
        if(userToDoList != null) {
            const myNewItem = await this.Items.create({
                ...newItem
            });
            await userToDoList.addItem(myNewItem);

            userToDoList = await user.getToDoList({
                include:[{model: this.Items, as: 'Items', attributes: ['name', "content"]}],
            })
            return userToDoList;
        }
        else {
            return null;
        }
    }

    public async removeItemToDoList(userId: string, itemId: string): Promise<ToDoListInstance | null> {
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }
        let userToDoList: ToDoListInstance | null = await user.getToDoList();
        if(userToDoList != null ) {
            const itemFoundInUserToDoList = await this.isItemInToDoList(itemId, userToDoList);
            if(itemFoundInUserToDoList != undefined) {
                await userToDoList.removeItem(itemFoundInUserToDoList);
                await this.Items.destroy({
                    where: {
                        id: itemId
                    }
                });
                userToDoList = await user.getToDoList({
                    include:[{model: this.Items, as: 'Items', attributes: ['name', "content"]}],
                })
                return userToDoList;
            }
            else {
                throw new ItemDeleteError("Item doesn't exist in user's todolist");
            }
        }
        else {
            // Didn't have a ToDoList
            return null;
        }
    }

    public async isItemInToDoList(itemId: string, userToDoList: ToDoListInstance): Promise<ItemInstance | undefined> {
        const items: ItemInstance[] = await userToDoList.getItems();
        return items.find((item) => {
            return item.id == parseInt(itemId);
        });
    }

    public async updateItemToDoList(userId: string, props: ItemCreationProps): Promise<ToDoListInstance | null> {
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }

        let userToDoList: ToDoListInstance | null = await user.getToDoList();
        if(userToDoList != null) {
            try {
                await this.Items.update({
                    ...props
                }, {
                    where: {
                        id: props.id,
                    }
                });
            } catch (e) {
                return null;
            }

            userToDoList = await user.getToDoList({
                include:[{model: this.Items, as: 'Items', attributes: ['name', "content"]}],
            })
            return userToDoList;
        }
        else {
            // Didn't have a ToDoList
            return null;
        }
    }


    async checkAddItem(item: IItemProperties, userId: string): Promise<boolean> {
        if (this.checkCaracDescr(item.content)) {
            throw new ToDoListAddItemError("limit of 1000 caracters in description");
        }
        if(await this.isFull(userId)) {
            throw new ToDoListAddItemError("TodoList is full, 10 items limit reached.");
        }
        if(!await this.isNameUnique(item, userId)) {
            throw new ToDoListAddItemError(`An item in this TodoList already has name ${item.name}.`);
        }
        if(!await this.isCreationDateCorrectSinceLastItem(userId)) {
            throw new ToDoListAddItemError(`Last item was created less than or 30 minutes ago. Please wait.`);
        }
        // this.handleEighthElement();
        return true;
    }

    checkCaracDescr(descr: string): boolean {
        return descr.length > 1000;
    }

    async isFull(userId: string): Promise<boolean> {
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }

        let userToDoList: ToDoListInstance | null = await user.getToDoList(
            {
                include:[{model: this.Items, as: 'Items', attributes: ['name', "content"]}],
            }
        );
        if(userToDoList != null) {
            const userItems: ItemInstance[] = await userToDoList.getItems();
            return userItems.length >= 10;
        }
        return true;
    }

    async isCreationDateCorrectSinceLastItem(userId: string): Promise<boolean> {
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }
        let userToDoList: ToDoListInstance | null = await user.getToDoList(
            {
                include:[{model: this.Items, as: 'Items', attributes: ['name', "content", 'createdAt']}],
            }
        );

        // @ts-ignore
        if(userToDoList == null || userToDoList.get('Items').length <= 0) {
            return true;
        }
        let nowDateMinus30min = new Date();
        nowDateMinus30min.setMinutes(nowDateMinus30min.getMinutes() -30);
        const items = userToDoList.get('Items');
        let lastItemCreationDate;

        // @ts-ignore
        if(typeof (items).toString() == 'object') {
            // @ts-ignore
            lastItemCreationDate = items.createdAt;
        } else {
            // @ts-ignore
            lastItemCreationDate = items[items.length - 1].createdAt;
        }
        lastItemCreationDate = new Date(lastItemCreationDate);


        return lastItemCreationDate <= nowDateMinus30min;
    }

    async isNameUnique(item: ItemCreationProps, userId: string): Promise<boolean> {
        const user: UserInstance | null = await this.findById(parseInt(userId));
        if(user == null) {
            throw new UserRetrivingException(`Couldn't find user ${userId} => sequelize error`);
        }
        let userToDoList: ToDoListInstance | null = await user.getToDoList(
            {
                include:[{model: this.Items, as: 'Items', attributes: ['name', "content", 'createdAt']}],
            }
        );

        const items = userToDoList.get('Items');
        // @ts-ignore
        if(items.length <= 0) {
            return true;
        }

        // @ts-ignore
        if(typeof (items).toString() == 'object') {
            // @ts-ignore
            return items.name != item.name;
        } else {
            // @ts-ignore
            const foundItemName = items.find((it) => {
                return it.name == item.name;
            });
            return foundItemName == undefined;
        }
    }

    // handleEighthElement(): boolean{
    //     if (this.items.length == 8) {
    //         EmailSenderService.sendEmail(this.user);
    //         return true;
    //     }
    //     return false;
    // }
    //
}
