import {ModelCtor} from "sequelize";
import {SequelizeManager} from "../models";
import {ToDoListCreationProps, ToDoListInstance} from "../models/toDoList.model";

export class ToDoListController {
    ToDoList: ModelCtor<ToDoListInstance>;

    private static instance: ToDoListController;

    public static async getInstance(): Promise<ToDoListController> {
        if(this.instance === undefined) {
            const manager = await SequelizeManager.getInstance();
            ToDoListController.instance = new ToDoListController(manager.ToDoList);
        }
        return ToDoListController.instance;

    }

    private constructor(User: ModelCtor<ToDoListInstance>) {
        this.ToDoList = User;
    }

    public async create(props: ToDoListCreationProps): Promise<[ToDoListInstance, boolean]> {
        return await this.ToDoList.findOrCreate({
            where: {
                ...props
            },
        });
    }

    public async delete(toDoListId: number): Promise<ToDoListInstance | null> {
        let toDoList: ToDoListInstance | null = await this.ToDoList.findOne({
            where: {
                id: toDoListId
            }
        });

        if(toDoList === null) {
            return null;
        }
        await toDoList.destroy();
        return toDoList;
    }
}
