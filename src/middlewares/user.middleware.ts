import Express from 'express';

export async function UserMiddlewareInsertIsValid(req: Express.Request, res: Express.Response, next: Express.NextFunction) {
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;
    const email: string = req.body.email;
    const password: string = req.body.password;
    const birthDate: Date = req.body.birthDate;
    if (lastName !== "" && firstName !== "" && email !== "" && password !== "" && birthDate != undefined) {
        if (isEmailValid(email) && isUserMoreThan13(birthDate) && isPasswordValid(password)) {
            next();
        }
    }
    res.status(403).end();
}


function isEmailValid(email: string): boolean {
    const emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegEx.test(String(email).toLowerCase());
}

function isPasswordValid(password: string): boolean {
    const passwordLength = password.length;
    return passwordLength >= 8 && passwordLength <= 40;
}

function isUserMoreThan13(birthDate: Date): boolean {
        const nowDateFormatted = new Date();
        const fullYearThirteenYearsAgo = nowDateFormatted.getFullYear() - 13;
        const dateThirteenYearsAgo: Date = new Date(nowDateFormatted.setFullYear(fullYearThirteenYearsAgo));

        return dateThirteenYearsAgo > birthDate;
}
