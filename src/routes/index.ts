import {Express} from 'express';
import {userRouter} from './User.route';

export function buildRoutes(app: Express) {
    app.use('/user', userRouter);
}
