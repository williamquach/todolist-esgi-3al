import Express from 'express';
import {UserController} from "../controllers/User.controller";
import {UserMiddlewareInsertIsValid} from "../middlewares/user.middleware";
import {ToDoListInstance} from "../models/toDoList.model";
import {ItemCreationProps} from "../models/item.model";
// import {Item} from "../models";

const userRouter = Express.Router();


userRouter.post('/', UserMiddlewareInsertIsValid, async (req, res) => {
    let user = req.body;
    const userController = await UserController.getInstance();
    userController.create(user).then((data) => {
        res.send(data);
    })
        .catch((error) => {
            console.log(error);
        });
});

userRouter.get('/:firstName', async (req, res) => {
    const userController = await UserController.getInstance();
    // @ts-ignore
    userController.findById(req.params.firstName)
        .then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
})

userRouter.put('/', async (req, res) => {
    const userController = await UserController.getInstance();
    await userController.update(req.body);
    res.status(200).json({message: "updated"})
})

userRouter.get('/', async (req, res) => {
    const userController = await UserController.getInstance();
    const start: number = parseInt(String(req.params.start)) || 0;
    const end: number = parseInt(String(req.params.end)) || 20;
    try {
        res.json({'data': await userController.findAll(start, end)});
    } catch (e) {
        res.status(500).end();
    }
})


// TODOLIST => Routes pour soutenance
userRouter.get('/:userId/toDoList', async (req, res) => {
    let userId = req.params.userId;
    if(userId == undefined) {
        res.status(400)
            .send(`Un paramètre dans le corps de la requête est manquant ! (userId)`)
            .end();
        return;
    }
    if(isNaN(Number(userId))) {
        res.status(400)
            .send(`Le paramètre userId dans le corps de la requête n'est pas de type entier !`)
            .end();
        return;
    }
    const userController = await UserController.getInstance();

    try {
        const toDoList: ToDoListInstance | null = await userController.getToDoList(userId);
        res.send(toDoList);
    }
    catch (exception) {
        res.status(403)
            .json(exception.toString())
            .end();
    }
});

userRouter.post('/:userId/toDoList', async (req, res) => {
    let userId = req.params.userId;
    let toDoListLabel = req.body.toDoListLabel;
    if(userId == undefined || toDoListLabel == undefined || toDoListLabel == "") {
        res.status(400)
            .send(`Un paramètre est manquant ! (userId ou toDoListLabel)`)
            .end();
        return;
    }
    if(isNaN(Number(userId))) {
        res.status(400)
            .send(`Le paramètre userId dans le corps de la requête n'est pas de type entier !`)
            .end();
        return;
    }

    const userController = await UserController.getInstance();

    try {
        const toDoList: ToDoListInstance | null = await userController.addToDoList(userId, toDoListLabel);
        if(toDoList == null) {
            res.send(toDoList);
        }
        else {
            res.status(201).send(toDoList);
        }
    }
    catch (exception) {
        res.status(403)
            .json(exception.toString())
            .end();
    }
});

userRouter.delete('/:userId/toDoList', async (req, res) => {
    let userId = req.params.userId;
    if(userId == undefined) {
        res.status(400)
            .send(`Un paramètre est manquant ! (userId)`)
            .end();
        return;
    }
    if(isNaN(Number(userId))) {
        res.status(400)
            .send(`Le paramètre userId dans le corps de la requête n'est pas de type entier !`)
            .end();
        return;
    }

    const userController = await UserController.getInstance();

    try {
        const removed: boolean = await userController.removeToDoList(userId);
        if(removed) {
            res.send("Deleted user's toDoList");
        }
        else {
            res.send("User didn't have a toDoList");
        }
    }
    catch (exception) {
        res.status(403)
            .json(exception.toString())
            .end();
    }
});


userRouter.post('/:userId/itemToDoList', async (req, res) => {
    let userId = req.params.userId;
    let item: ItemCreationProps = req.body;
    if(userId == undefined || item.name == undefined || item.content == undefined || item.name == "" && item.content.length > 1000) {
        res.status(400)
            .send(`Un paramètre est manquant ! (userId ou toDoListLabel)`)
            .end();
        return;
    }
    const userController = await UserController.getInstance();

    try {
        const toDoList: ToDoListInstance | null = await userController.addItemToDoList(userId, item);
        if(toDoList == null) {
            res.status(500).end();
        }
        else {
            res.status(201).send(toDoList);
        }
    }
    // TODO : si l'utilisateur n'a pas de todolist => ça renvoie Validation error items > 10
    catch (exception) {
        res.status(403)
            .json(exception.toString())
            .end();
    }
});

userRouter.put('/:userId/itemToDoList', async (req, res) => {
    const userController = await UserController.getInstance();
    let userId = req.params.userId;
    let item: ItemCreationProps = req.body;
    if(userId == undefined || item.name == "" && item.content.length > 1000) {
        res.status(400)
            .send(`Un paramètre est erroné ! (userId ou l'item)`)
            .end();
        return;
    }

    try {
        const toDoList: ToDoListInstance | null = await userController.updateItemToDoList(userId, item);
        if(toDoList == null) {
            res.status(500);
        }
        else {
            res.status(200).json({
                message: "updated",
                data: toDoList
            })
        }
    }
    catch (exception) {
        res.status(403)
            .json(exception.toString())
            .end();
    }

})

userRouter.delete('/:userId/:itemId/itemToDoList', async (req, res) => {
    const userController = await UserController.getInstance();
    let userId = req.params.userId;
    let itemId = req.params.itemId;
    if(userId == undefined || itemId == undefined) {
        res.status(400)
            .send(`Un paramètre est erroné ! (userId ou itemId)`)
            .end();
        return;
    }
    try {
        const toDoList: ToDoListInstance | null = await userController.removeItemToDoList(userId, itemId);
        if(toDoList == null) {
            res.status(500)
                .send("L'utilisateur n'a pas de ToDoList.")
                .end();
        }
        else {
            res.status(200).json({
                message: "updated",
                data: toDoList
            })
        }
    }
    catch (exception) {
        res.status(403)
            .json(exception.toString())
            .end();
    }
})

export {
    userRouter
}
