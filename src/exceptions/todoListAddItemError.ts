import {ValidationError} from "./validationError";

export class ToDoListAddItemError extends ValidationError {
    constructor(message: string) {
        super(message);
        this.name = this.constructor.name;
        Object.setPrototypeOf(this, ToDoListAddItemError.prototype);
    }
}
