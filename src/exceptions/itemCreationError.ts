import {ValidationError} from "./validationError";

export class ItemCreationError extends ValidationError {
    constructor(message: string) {
        super(message);
        this.name = this.constructor.name;
        Object.setPrototypeOf(this, ItemCreationError.prototype);
    }
}
