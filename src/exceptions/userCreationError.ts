import {ValidationError} from "./validationError";

export class UserCreationError extends ValidationError {
    constructor(message: string) {
        super(message);
        this.name = this.constructor.name;
        Object.setPrototypeOf(this, UserCreationError.prototype);
    }
}
