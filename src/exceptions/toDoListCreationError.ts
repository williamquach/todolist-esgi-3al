import {ValidationError} from "./validationError";

export class ToDoListCreationError extends ValidationError {
    constructor(message: string) {
        super(message);
        this.name = this.constructor.name;
        Object.setPrototypeOf(this, ToDoListCreationError.prototype);
    }
}
