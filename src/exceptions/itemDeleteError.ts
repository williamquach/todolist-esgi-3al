import {ValidationError} from "./validationError";

export class ItemDeleteError extends ValidationError {
    constructor(message: string) {
        super(message);
        this.name = this.constructor.name;
        Object.setPrototypeOf(this, ItemDeleteError.prototype);
    }
}
