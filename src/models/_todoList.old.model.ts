import {Item} from "./_item.old.model";
import {ToDoListCreationError} from "../exceptions/toDoListCreationError";
import {EmailSenderService} from "../services/EmailSenderService";
import {ToDoListAddItemError} from "../exceptions/todoListAddItemError";
import {User} from "./_user.old.model";

export interface ITodoListProperties {
    label: string;
    items?: Item[];
    user: User;
}

export class TodoList {
    label: string;
    items: Item[];
    user: User;

    constructor(props: ITodoListProperties) {
        this.label = props.label;
        this.items = props.items != undefined ? props.items : [];
        this.user = props.user;
    }

    static create(props: ITodoListProperties): TodoList {
        if(TodoList.isValidInsert(props)) {
            return new TodoList(props);
        }
        throw new ToDoListCreationError("ToDoList properties are not valid, so ToDoList couldn't be created.");
    }

    static isValidInsert(props: ITodoListProperties): boolean {
        let itemListNotFull = true;
        if(props.items != undefined) {
            itemListNotFull = props.items.length <= 10;
        }
        return props.label != "" && itemListNotFull;
    }

    addItem(item: Item): boolean {
        if(this.isFull()) {
            throw new ToDoListAddItemError("TodoList is full, 10 items limit reached.");
        }
        if(!this.isItemUnique(item)) {
            throw new ToDoListAddItemError("This item is already in this TodoList.");
        }
        if(!this.isNameUnique(item)) {
            throw new ToDoListAddItemError(`An item in this TodoList already has name ${item.name}.`);
        }
        if(!Item.isValidInsert(item)) {
            throw new ToDoListAddItemError("Tried to add an invalid item.");
        }
        if(!this.isCreationDateCorrectSinceLastItem()) {
            throw new ToDoListAddItemError(`Last item was created less than or 30 minutes ago. Please wait.`);
        }
        this.items!.push(item);
        this.handleEighthElement();
        return true;
    }

    isFull(): boolean {
        return this.items.length >= 10;
    }

    isCreationDateCorrectSinceLastItem(): boolean {
        if(this.items == undefined || this.items.length <= 0) {
            return true;
        }
        let nowDateMinus30min = new Date();
        nowDateMinus30min.setMinutes(nowDateMinus30min.getMinutes() -30);
        let lastItemCreationDate = this.items[this.items.length - 1].createdAt;

        return lastItemCreationDate <= nowDateMinus30min;
    }

    handleEighthElement(): boolean{
        if (this.items.length == 8) {
            EmailSenderService.sendEmail(this.user);
            return true;
        }
        return false;
    }

    isNameUnique(searchedItem: Item): boolean {
        if(this.items.length <= 0) {
            return true;
        }
        const foundItemName = this.items.find((item) => {
            return item.name == searchedItem.name;
        });
        return foundItemName == undefined;
    }

    isItemUnique(searchedItem: Item): boolean {
        return this.items.indexOf(searchedItem) == -1;
    }
}
