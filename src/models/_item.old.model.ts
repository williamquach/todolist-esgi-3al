import {ItemCreationError} from "../exceptions/itemCreationError";

export interface IItemProperties {
    name: string;
    content: string;
}

export class Item {
    name: string;
    content: string;
    createdAt: Date;

    constructor(props: IItemProperties) {
        this.name = props.name;
        this.content = props.content;
        this.createdAt = new Date();
    }

    static create(props: IItemProperties): Item {
        if(this.isValidInsert(props)) {
            return new Item(props);
        }
        throw new ItemCreationError("Item properties are not valid, so item couldn't be created.");
    }

    static isValidInsert(props: IItemProperties): boolean {
        return props.name != "" && props.content.length <= 1000;
    }
}
