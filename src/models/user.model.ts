import {
    Sequelize,
    Model,
    Optional,
    DataTypes,
    ModelCtor, HasOneGetAssociationMixin, HasOneSetAssociationMixin,
} from "sequelize";
import {ToDoListInstance} from "./toDoList.model";

export interface IUserProperties {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    password: string; // Not encrypted
    birthDate: Date;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}


export interface UserCreationProps extends Optional<IUserProperties, "id" | "createdAt" | "updatedAt" | "deletedAt"> {}

export interface UserInstance extends Model<IUserProperties, UserCreationProps>, IUserProperties {
    getToDoList: HasOneGetAssociationMixin<ToDoListInstance>;
    setToDoList: HasOneSetAssociationMixin<ToDoListInstance, "id">
}

export default function (sequelize: Sequelize): ModelCtor<UserInstance> {
    return sequelize.define<UserInstance>("User", {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: {
            type: DataTypes.STRING,
        },
        lastName: {
            type: DataTypes.STRING,
        },
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
        },
        birthDate: {
            type: DataTypes.DATE,
        },
        createdAt: { type: DataTypes.DATE, field: 'created_at' },
        updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
        deletedAt: { type: DataTypes.DATE, field: 'deleted_at' }
    }, {
        freezeTableName: true,
        underscored: true,
        paranoid: true,
        timestamps: true
    });
}
