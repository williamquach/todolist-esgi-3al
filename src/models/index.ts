import {ModelCtor, Sequelize} from "sequelize";
import userCreator, {UserInstance} from "./user.model";
import toDoListCreator, {ToDoListInstance} from "./toDoList.model";
import itemCreator, {ItemInstance} from "./item.model";

export * from './_item.old.model';
export * from './_user.old.model';
export * from './_todoList.old.model';

export interface SequelizeManagerProps {
    sequelize: Sequelize;
    User: ModelCtor<UserInstance>;
    ToDoList: ModelCtor<ToDoListInstance>;
    Item: ModelCtor<ItemInstance>;
}


export class SequelizeManager implements SequelizeManagerProps {
    private static instance?: SequelizeManager;

    sequelize: Sequelize;
    User: ModelCtor<UserInstance>;
    ToDoList: ModelCtor<ToDoListInstance>;
    Item: ModelCtor<ItemInstance>;

    public static async getInstance(): Promise<SequelizeManager> {
        if(SequelizeManager.instance === undefined) {
            SequelizeManager.instance = await SequelizeManager.initialize();
        }
        return SequelizeManager.instance;
    }

    private static async initialize(): Promise<SequelizeManager> {
        const sequelize = new Sequelize('sqlite::memory:');
        // const sequelize = new Sequelize({
        //     dialect: process.env.DB_DRIVER as Dialect,
        //     host: process.env.DB_HOST,
        //     database: process.env.DB_NAME,
        //     username: process.env.DB_USER,
        //     password: process.env.DB_PASSWORD,
        //     port: Number.parseInt(process.env.DB_PORT as string),
        //     logging: false
        // });
        await sequelize.authenticate();

        const managerProps: SequelizeManagerProps = { // liste des modèles
            sequelize,
            User: userCreator(sequelize),
            ToDoList: toDoListCreator(sequelize),
            Item: itemCreator(sequelize),
        }

        SequelizeManager.associate(managerProps);

        // await sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
        await sequelize.sync({
            force: true
        });
        // await sequelize.query('SET FOREIGN_KEY_CHECKS = 1'); // setting the flag back for security

        return new SequelizeManager(managerProps);
    }

    private static associate(props: SequelizeManagerProps): void {
        props.User.hasOne(props.ToDoList);
        props.ToDoList.belongsTo(props.User);
        props.ToDoList.hasMany(props.Item);
        props.Item.belongsTo(props.Item);
    }

    constructor(props: SequelizeManagerProps) {
        this.sequelize = props.sequelize;
        this.User = props.User;
        this.ToDoList = props.ToDoList;
        this.Item = props.Item;
    }
}
