import {
    Sequelize,
    Model,
    Optional,
    DataTypes,
    ModelCtor, BelongsToGetAssociationMixin,
} from "sequelize";
import {ToDoListInstance} from "./toDoList.model";

export interface IItemProperties {
    id?: number;
    name: string;
    content: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}

export interface ItemCreationProps extends Optional<IItemProperties, "id" | "createdAt" | "updatedAt" | "deletedAt"> {}

export interface ItemInstance extends Model<IItemProperties, ItemCreationProps>, IItemProperties {
    getToDoList: BelongsToGetAssociationMixin<ToDoListInstance>;
}

export default function (sequelize: Sequelize): ModelCtor<ItemInstance> {
    return sequelize.define<ItemInstance>("Item", {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
        },
        content: {
            type: DataTypes.STRING,
        },
        createdAt: { type: DataTypes.DATE, field: 'created_at' },
        updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
        deletedAt: { type: DataTypes.DATE, field: 'deleted_at' }
    }, {
        freezeTableName: true,
        underscored: true,
        paranoid: true,
        timestamps: true
    });
}
