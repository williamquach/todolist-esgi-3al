import {UserCreationError} from "../exceptions";
import {ToDoListCreationError} from "../exceptions/toDoListCreationError";
import {ITodoListProperties, TodoList} from "./_todoList.old.model";

export interface IUserProperties {
    firstName: string;
    lastName: string;
    email: string;
    password: string; // Not encrypted
    birthDate: Date;
    toDoList?: TodoList;
}

export class User {
    firstName: string;
    lastName: string;
    email: string;
    password: string; // Not encrypted, between 8 & 40 characters
    birthDate: Date;
    toDoList?: TodoList;

    private constructor(props: IUserProperties) {
        this.firstName = props.firstName;
        this.lastName = props.lastName;
        this.email = props.email;
        this.password = props.password;
        this.birthDate = props.birthDate;
    }

    static create(props: IUserProperties): User {
        if(this.isValid(props)) {
            return new User(props);
        }
        throw new UserCreationError("User properties are not valid, so user couldn't be created.");
    }

    static isValid(props: IUserProperties): boolean {
        const firstName: string = props.firstName;
        const lastName: string = props.lastName;
        const email: string = props.email;
        const password: string = props.password;
        const birthDate: Date = props.birthDate;
        if(lastName !== "" && firstName !== "" && email !== "" && password !== "" && birthDate != undefined) {
            if(this.isEmailValid(email) && this.isUserMoreThan13(birthDate) && this.isPasswordValid(password)) {
                return true;
            }
        }
        return false;
    }

    static isEmailValid(email: string): boolean {
        const emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailRegEx.test(String(email).toLowerCase());
    }

    static isPasswordValid(password: string): boolean {
        const passwordLength = password.length;
        return passwordLength >= 8 && passwordLength <= 40;
    }

    static isUserMoreThan13(birthDate: Date): boolean {
        const nowDateFormatted = new Date();
        const fullYearThirteenYearsAgo = nowDateFormatted.getFullYear() - 13;
        const dateThirteenYearsAgo: Date = new Date(nowDateFormatted.setFullYear(fullYearThirteenYearsAgo));

        return dateThirteenYearsAgo > birthDate;
    }

    createToDoList(props: ITodoListProperties): TodoList {
        if(this.toDoList != undefined) {
            throw new ToDoListCreationError("User already has a TodoList.");
        }
        if(!User.isValid(this)) {
            throw new ToDoListCreationError("An invalid user cannot create a TodoList. Please see user properties.");
        }
        return TodoList.create(props);
    }
}
