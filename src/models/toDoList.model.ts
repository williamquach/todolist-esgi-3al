import {
    Sequelize,
    Model,
    Optional,
    DataTypes,
    ModelCtor,
    BelongsToGetAssociationMixin,
    HasManyAddAssociationMixin,
    HasManyGetAssociationsMixin,
    HasManyRemoveAssociationMixin, BelongsToSetAssociationMixin,
} from "sequelize";
import {UserInstance} from "./user.model";
import {ItemInstance} from "./item.model";

export interface IToDoListProperties {
    id: number;
    label: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}

export interface ToDoListCreationProps extends Optional<IToDoListProperties, "id" | "createdAt" | "updatedAt" | "deletedAt"> {}

export interface ToDoListInstance extends Model<IToDoListProperties, ToDoListCreationProps>, IToDoListProperties {
    getUser: BelongsToGetAssociationMixin<UserInstance>;
    setUser: BelongsToSetAssociationMixin<UserInstance, "id">
    getItems: HasManyGetAssociationsMixin<ItemInstance>;
    addItem: HasManyAddAssociationMixin<ItemInstance, "id">
    removeItem: HasManyRemoveAssociationMixin<ItemInstance, "id">;
}

export default function (sequelize: Sequelize): ModelCtor<ToDoListInstance> {
    return sequelize.define<ToDoListInstance>("ToDoList", {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        label: {
            type: DataTypes.STRING,
        },
        createdAt: { type: DataTypes.DATE, field: 'created_at' },
        updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
        deletedAt: { type: DataTypes.DATE, field: 'deleted_at' }
    }, {
        freezeTableName: true,
        underscored: true,
        paranoid: true,
        timestamps: true
    });
}
