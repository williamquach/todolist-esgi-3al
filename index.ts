import {config} from "dotenv";
config();
import {SequelizeManager} from "./src/models";
import {buildRoutes} from "./src/routes";
import Express from 'express';
let cors = require('cors');

async function main(): Promise<void> {
    const {
        User,
        ToDoList,
        Item
    } = await SequelizeManager.getInstance();

    const u1 = await User.create({
        firstName: 'f',
        lastName: 'f',
        birthDate: new Date(),
        password: 't',
        email: 'f@gmail.com'
    })

    // const userQueried: UserInstance | null = await User.findOne({
    //     where: {
    //         id: 1
    //     }
    // });
    // if(userQueried != null) {
    //     console.log(userQueried);
    //     console.log(userQueried.id);
    //     console.log(userQueried.firstName);
    //     console.log(userQueried.lastName);
    //     console.log(userQueried.email);
    // }
}


main().then(function() {
    console.log("Main OK");
}).catch(function (err) {
    console.error(err);
    console.log("Error sequelize manager");
});


const app = Express();

app.use(Express.urlencoded({extended: true}));
app.use(Express.json());
app.use(cors({origin: 'http://localhost:4200'}));

buildRoutes(app);

const port = process.env.PORT || 3000;
app.listen(port, function () {
    console.log(`Listening on ${port}...`);
});
